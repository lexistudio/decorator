import Vue from 'vue'
import { Action, Module, Mutation, MutationAction, VuexModule } from "vuex-module-decorators";
import { Module as Mod } from "vuex";
import Url from '@/utils/url'

// Interface TS
declare interface Post {
  title?: string,
  id?: number,
  body?: string
}

// Module
@Module
export default class Posts extends VuexModule {
  constructor( module: Mod<ThisType<{}>, any> ) {
    super(module)
  }

  // State
  list: Post = {}

  // Getter
  get getPost():any {
    return this.list;
  }

  // Mutation
  @Mutation
  setPost(payload: any): void {
    return this.list = payload
  }

  // Action
  @Action({rawError: true})
  addPost(params: any): void {
    const obj = Object.assign({ _limit: 2 }, params)

    Vue.axios.get(Url.post, { params: obj }).then(resp => {
      this.context.commit('setPost', resp.data)
    })
  }
}