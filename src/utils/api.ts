import Axios from 'axios'

Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/'

Axios.interceptors.request.use(
  config => {
    const token = localStorage.getItem('user-token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

Axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    const originalRequest = error.config;
    const expires = localStorage.getItem('expires');
    const expiresDate = 'unix settings';
    const now = 'date settings';

    if ( error.response.status === 404 ) {
      alert('404');
    }

    if (  error.response.status === 401 && expires && now >= expiresDate && !originalRequest._retry ) {
      originalRequest._retry = true

      // return ' Для refresh token '
    }
    else {
      return Promise.reject(error)
    }
  }
)

export default Axios