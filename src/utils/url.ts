export default {
  post: 'posts',
  comments: 'comments',
  albums: 'albums',
  photos: 'photos',
  todos: 'todos',
  users: 'users',
}