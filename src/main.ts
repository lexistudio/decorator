// Import
import Vue from 'vue'
import App from './App.vue'
import store from './store'
import axios from './utils/api'
import VueAxios from 'vue-axios'
import { loadProgressBar } from 'axios-progress-bar'
import VueSimplebar from 'vue-simplebar'
import Paginate from 'vuejs-paginate'

// Style
import './css/site.styl';

// Settings
Vue.config.productionTip = false

// Use Plugin
Vue.use(VueSimplebar)
Vue.use(VueAxios, axios)
Vue.use(loadProgressBar)

// Local Component
Vue.component('Paginate', Paginate)

// Init
new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
