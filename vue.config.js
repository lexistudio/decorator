module.exports = {
  outputDir: './dist',
  assetsDir: './assets/',
  configureWebpack: {
    stats: {
      warnings: false,
      errors: false
    }
  },
  css: {
    requireModuleExtension: true
  },
};
